package iut_orsay.simon.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import iut_orsay.simon.MainActivity;
import iut_orsay.simon.R;

public class MenuFragment extends Fragment {
    private String chatLog;
    private TextView textScore;
    private ScrollView scroll;

    public MenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        View v = inflater.inflate(R.layout.fragment_menu, container, false);

        textScore = (TextView) v.findViewById(R.id.textScore);
        scroll = (ScrollView) v.findViewById(R.id.scrollScore);
        chatLog = "";
        final ArrayList<Integer> list = MainActivity.scores;
        int num =1;
        for (int i = list.size() - 1; i >= 0; i--){
            displayMessage(num, list.get(i));
            num++;
        }
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void displayMessage(int num, int score) {
        chatLog += "#" + num + ": " + score + "\n";
        textScore.setText(chatLog);
        scroll.fullScroll(View.FOCUS_DOWN);
    }

}
