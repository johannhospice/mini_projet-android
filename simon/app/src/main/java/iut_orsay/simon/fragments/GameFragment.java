package iut_orsay.simon.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import android.media.MediaPlayer;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import iut_orsay.simon.MainActivity;
import iut_orsay.simon.R;

public class GameFragment extends Fragment {
    private final Round roundThread = new Round();
    private ArrayList<MediaPlayer> sounds;
    private ArrayList<Integer> sequence;
    private ArrayList<Button> buttons;
    private TextView scoreDisplay;
    public static int score = 0;
    private int currentSP = 0;
    private Handler handler;

    public GameFragment() {
        super();
        buttons = new ArrayList<>();
        sounds = new ArrayList<>();
        sequence = new ArrayList<>();
        handler = new Handler();
        roundThread.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        View v = inflater.inflate(R.layout.fragment_game, container, false);

        sounds.clear();
        sounds.add(MediaPlayer.create(this.getActivity(), R.raw.blue));
        sounds.add(MediaPlayer.create(this.getActivity(), R.raw.red));
        sounds.add(MediaPlayer.create(this.getActivity(), R.raw.yellow));
        sounds.add(MediaPlayer.create(this.getActivity(), R.raw.green));

        buttons.clear();
        buttons.add((Button) v.findViewById(R.id.blue));
        buttons.add((Button) v.findViewById(R.id.red));
        buttons.add((Button) v.findViewById(R.id.yellow));
        buttons.add((Button) v.findViewById(R.id.green));

        for (int i = 0; i < buttons.size(); i++) {
            final int fi = i;
            buttons.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sounds.get(fi).start();
                    if (verify_click(fi))
                        roundThread.exec();
                }
            });
        }
        v.findViewById(R.id.pausebtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        scoreDisplay = (TextView) v.findViewById(R.id.score);
        updateScore();
        return v;
    }

    public boolean verify_click(int btnNum) {
        if (btnNum == sequence.get(currentSP)) {
            if (currentSP == sequence.size() - 1) {
                currentSP = 0;
                score++;
                updateScore();
                return true;
            }
            currentSP++;
        } else {
            finish();
        }
        return false;
    }

    private void finish() {
        MainActivity.addScore(score);
        ((MainActivity) getActivity()).save();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.fragment, MainActivity.gameoverFragment);
        transaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        score = 0;
        currentSP = 0;
        sequence.clear();
        roundThread.exec();
        updateScore();

    }

    public void updateScore() {
        scoreDisplay.setText(score + "");
    }

    public int rand(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    public class Round extends Thread {
        private boolean run = false;
        private int quick = 1;

        @Override
        public void run() {
            super.run();
            while (true) {
                if (run) {
                    if (sequence.size() < 15) quick *= 1.1;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            for (Button btn : buttons)
                                btn.setEnabled(false);
                        }
                    });
                    for (int i = 0; i < sequence.size(); i++)
                        fakeClick(sequence.get(i));
                    int val = rand(0, 3);
                    sequence.add(val);
                    fakeClick(val);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            for (Button btn : buttons)
                                btn.setEnabled(true);
                        }
                    });
                    run = false;
                }
            }
        }

        public void fakeClick(final int i) {
            try {
                Thread.sleep(500 / quick);
                sounds.get(i).start();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        buttons.get(i).setPressed(true);
                    }
                });
                Thread.sleep(1000 / quick);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        buttons.get(i).setPressed(false);
                    }
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void exec() {
            run = true;
        }

    }
}