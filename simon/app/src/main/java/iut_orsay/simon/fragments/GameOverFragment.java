package iut_orsay.simon.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import iut_orsay.simon.R;

public class GameOverFragment extends Fragment {
    public GameOverFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_game_over, container, false);
        ((TextView) v.findViewById(R.id.score_game_over)).setText("" + GameFragment.score);
        return v;
    }

}
