package iut_orsay.simon;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import iut_orsay.simon.fragments.GameFragment;
import iut_orsay.simon.fragments.GameOverFragment;
import iut_orsay.simon.fragments.MainFragment;
import iut_orsay.simon.fragments.MenuFragment;
import iut_orsay.simon.tools.ObjectSerializer;

public class MainActivity extends AppCompatActivity {
    public MainFragment mainFragment = new MainFragment();
    public MenuFragment menuFragment = new MenuFragment();
    public GameFragment gameFragment = new GameFragment();
    public static GameOverFragment gameoverFragment = new GameOverFragment();
    public static ArrayList<Integer> scores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        toggleFragment(mainFragment);
        load();
    }

    @Override
    @SuppressLint("InlinedApi")
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            findViewById(R.id.fullscreen_content).setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void startMain(View view) {
        toggleFragment(mainFragment);
    }

    public void startGame(View view) {
        toggleFragment(gameFragment);
    }

    public void startMenu(View view) {
        toggleFragment(menuFragment);
    }

    public void toggleFragment(Fragment frag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.fragment, frag);
        transaction.commit();
    }

    public static void addScore(int score) {
        scores.add(score);
        if (scores.size() > 30)
            scores.remove(scores.size() - 1);
        Collections.sort(scores);
    }

    public void save() {
        try {
            SharedPreferences sharedPref = getSharedPreferences("Score", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("ScoreList", ObjectSerializer.serialize(scores));
            editor.commit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            SharedPreferences prefs = getSharedPreferences("Score", Context.MODE_PRIVATE);
            scores = (ArrayList<Integer>) ObjectSerializer.deserialize(prefs.getString("ScoreList", ObjectSerializer.serialize(new ArrayList())));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
