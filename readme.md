# Mini-projet #

L'objectif est de réaliser une application Android implantant le jeu de société [Simon](https://fr.wikipedia.org/wiki/Simon_%28jeu%29). Le but de ce jeu de mémoire est de reproduire une séquence de touches de différentes couleurs de plus en plus longue, sans se tromper.

L'application doit suivre [cette description du jeu](https://fr.wikipedia.org/wiki/Simon_%28jeu%29), à la différence qu'il n'y a pas de limite de temps pour reproduire la séquence.

## Travail demandé ##

Le mini-projet est à faire en binôme, les binômes étant dans le même groupe de TP. Si un groupe comporte un nombre impair d'étudiants, il y aura un trinôme. Essayez de vous organiser pour répartir les tâches, tout en comprenant la partie de l'autre ou des autres personnes !

Il vous est demandé de :

1. Concevoir une interface agréable à utiliser pour ce jeu. Cette interface doit se dessiner correctement sur différentes tailles de tablettes et avec différentes orientations. Elle doit être disponible en anglais et en français.
2. Programmer le jeu correspondant à l'interface. Un grand soin doit être apporté à la robustesse : l'application ne doit pas crasher ; en cas d'erreur, un message peut être affiché au joueur, mais l'application doit continuer, éventuellement en se réinitialisant.
3. Apporter votre touche personnelle à l'application : choisissez une amélioration qui vous paraît intéressante et implantez-là. Quelques exemples possibles : plusieurs niveaux de difficultés, choix des couleurs, version pour personnes daltoniennes, ...

Vous devrez expliquer votre approche dans un petit rapport spécifiant :

1. comment utiliser l'application ;
2. votre touche personnelle ;
3. les choix de programmation intéressants que vous avez faits.

Évitez d'écrire un rapport "scolaire" !

## Rendu ##

Code et rapport sont à rendre par mail à votre enseignant de TP pour le samedi 12 mars à 23h59.

## Conseils ##

Pour travailler efficacement à plusieurs, je vous conseille d'utiliser un [gestionnaire de suivi de version](https://fr.wikipedia.org/wiki/Gestion_de_versions). [Github](https://github.com/) offre par exemple un moyen gratuit de stocker son dépôt, et il existe de nombreux tutoriels sur le net pour l'utiliser. Même si c'est la première fois que vous en utilisez un, cela vous fera sans doute gagner du temps :

* Pas besoin de s'échanger les projets par mail : en plus du problème de la taille de ces projets, on ne risque pas de se tromper en ne prenant pas la dernière version !
* Vous pouvez éditer en même temps chacun de votre côté, il est facile ensuite de fusionner.
* Vous avez accès à l'historique de tout ce que vous avez essayé.